// console.log(`Hello po`);

// EXPONENT OPERATOR
// before ES6
const firstNum = 8 ** 2;
console.log(firstNum);

// ES6
const secondNum = Math.pow(8, 2);
console.log(secondNum);

// TEMPLATE LITERALS
// allows to write strings without using concatenation
// helps with code readability
// allows us to write strings with embedded JavaScript

let name = "John";
// before ES6
let message = "Hello, " + name + "! Welcome to programming!";
console.log(message);

// ES6
message = `Hello, ${name}! 
Welcome 
to 
programming!`;
console.log(message);

const interestRate = 0.1;
const principal = 1000;
console.log(
  `The interest on your savings account is: ${interestRate * principal}`
);

// ARRAY DESTRUCTURING
// allows us to unpack elements in arrays into distinct variables instead of using index numbers
// helps in code readability
// Syntax:
// let/const [variableNameA, variableNameB, ...] = arrayName;

const fullName = ["Juan", "Dela", "Cruz"];
// before array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(
  `Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you.`
);

const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(fullName);
console.log(
  `Hello ${firstName} ${middleName} ${lastName}! It is nice to meet you!`
);

// OBJECT DESTRUCTURING
// allows us to unpack properties of objects into variables
// Syntax:
// let/const { propertyNameA, propertyNameB, propertyNameC, . . .} = objectName;

const person = {
  givenName: "Jane",
  maidenName: "Dela",
  familyName: "Cruz",
};
// before object destructuring
console.log(
  `Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It is nice to meet you!`
);

const { givenName, maidenName, familyName } = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(person);

function getFullName({ givenName, maidenName, familyName }) {
  console.log(
    `Hello ${givenName} ${maidenName} ${familyName}! It is nice to meet you!`
  );
}
getFullName(person);

// ARROW FUNCTIONS
// compacts alternative syntax to traditional functions
// useful for code snippets where creating functions will not be reused

const hello = () => {
  console.log(`Hello World!`);
};
hello();

// before arrow and template literals
function printFullName(firstName, middleInitial, lastName) {
  console.log(firstName + " " + middleInitial + " " + lastName);
}

printFullName("EJ", "P.", "Urbina");

let fullName2 = (firstName, middleInitial, lastName) => {
  console.log(`${firstName} ${middleInitial} ${lastName}`);
};

fullName2("EJ", "P.", "Urbina");

// Arrow Function with loops
let students = ["John", "Jane", "Judy"];

// before arrow function
students.forEach(function (student) {
  console.log(student + " is a student.");
});

// ARROW FUNCTION SOLUTION
let studentFunction = (student) => {
  console.log(`${student} is a student`);
};
students.forEach(studentFunction);

// Another solution
students.forEach((student) => {
  console.log(`${student} is a student`);
});

// IMPLICIT RETURN STATEMENT
// there are instances when you can omit return statement
// this works because even without return statement, JavaScript implicitly adds it for the result of the function

const add = (x, y) => {
  console.log(x + y);
  return x + y;
};
let sum = add(23, 45);
console.log(sum);

const subtract = (x, y) => x - y; // arrow function
let difference = subtract(243, 55);
console.log(difference);

// Default Function Argument Value
// provide a default argument value if none is provided when the function is invoked

const greet = (name = "User") => {
  return `Good morning, ${name}!`;
};
console.log(greet());

// CLASS-BASED OBJECT BLUEPRINTS
// allows us to create/instantiation of objects using classes blueprints
// creating a class
// constructor is a special method of a class for creating/initializing an object for that class
// Syntax:
// class className {
//   constructor(objectValueA, objectValueB, ...) {
//     this.objectPropertyA = objectValueA;
//     this.objectPropertyB = objectValueB;
//   };
// }

class Car {
  constructor(brand, name, year) {
    this.carBrand = brand;
    this.carName = name;
    this.carYear = year;
  }
}

let car1 = new Car("Toyota", "Vios", "1993");
console.log(car1);
